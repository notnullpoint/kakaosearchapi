package com.thty.kakaosearchapi.viewmodel

import android.app.Application
import android.view.View
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.thty.kakaosearchapi.base.BaseViewModel
import com.thty.kakaosearchapi.core.net.ApiService
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.data.Meta
import com.thty.kakaosearchapi.view.adapter.SearchAdapter
import com.thty.kakaosearchapi.view.listener.OnBookClick
import kotlinx.coroutines.*


class SearchViewModel(app : Application, private val api: ApiService): BaseViewModel(app) {

    val activityToStart = MutableLiveData<Pair<View, String>>()
    val toastMessage = MutableLiveData<String>()

    val adapter: SearchAdapter by lazy {
        SearchAdapter(object : OnBookClick {
            override fun onClick(view: View, document: Document) {
                view.tag = document
                activityToStart.value = Pair(view, ViewCompat.getTransitionName(view)!!)
            }
        })
    }

    var isLoading = MutableLiveData<Boolean>(false)
    var meta = Meta()

    var searchText = ""
    var isIconVisible = MutableLiveData<Boolean>(true)

    fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        isIconVisible.value = s.isNullOrEmpty()
    }

    fun getSearchApi(page: Int){
        if(page == 1){
            adapter.clear()
        }

        viewModelScope.launch {
            val documents = withContext(Dispatchers.IO) {
                api.getBooks(searchText, page, 20)
            }
            meta = documents.meta ?: Meta()
            adapter.addItem(documents.documents)
            isLoading.value = false
            if (meta.totalCount == 0) {
                toastMessage.value = "검색 결과가 없습니다."
            } else if (meta.isEnd) {
                toastMessage.value = "마지막 페이지 입니다."
            }
        }
    }

    fun isLastPage(): Boolean {
        return meta.isEnd
    }

}