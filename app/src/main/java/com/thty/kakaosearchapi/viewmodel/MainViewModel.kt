package com.thty.kakaosearchapi.viewmodel

import android.app.Application
import android.view.View
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.thty.kakaosearchapi.R
import com.thty.kakaosearchapi.base.BaseViewModel
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.data.room.BookDao
import com.thty.kakaosearchapi.view.adapter.MainAdapter
import com.thty.kakaosearchapi.view.listener.OnBookClick
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel(app : Application, var dao: BookDao): BaseViewModel(app) {

    val activityToStart = MutableLiveData<Pair<View, String>>()

    val adapter: MainAdapter by lazy {
        MainAdapter(object : OnBookClick {
            override fun onClick(view: View, document: Document) {
                view.tag = document
                activityToStart.value = Pair(view, ViewCompat.getTransitionName(view)!!)
            }
        })
    }

    init {
        getLatelyBooks()
    }

    private val constHint: String by lazy {
        app.getString(R.string.search_hint)
    }

    var hintTxt = MutableLiveData<String>("")


    fun startTyping(){
        viewModelScope.launch{
            for(letter in  constHint){
                delay(100)
                hintTxt.value += letter
            }
        }
    }

    fun getLatelyBooks(){
        viewModelScope.launch {
            adapter.setItem(dao.getDocuments() as ArrayList<Document>)
        }
    }

}