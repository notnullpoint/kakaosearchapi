package com.thty.kakaosearchapi.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.thty.kakaosearchapi.base.BaseViewModel
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.data.room.BookDao
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class DetailViewModel(app : Application, var dao: BookDao): BaseViewModel(app) {

    fun getPrice(price: Int): String{
        return String.format("%,d", price) + "원"
    }

    fun getAuthorsName(list: ArrayList<String>): String {
        return list.joinToString(",")
    }

    fun insertDocument(document: Document) = viewModelScope.launch{
            document.creationDate = Date(System.currentTimeMillis())
            dao.insert(document)
    }

}