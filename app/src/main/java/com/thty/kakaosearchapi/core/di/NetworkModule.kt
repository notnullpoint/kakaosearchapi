package com.thty.kakaosearchapi.core.di

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.thty.kakaosearchapi.BuildConfig
import com.thty.kakaosearchapi.core.net.ApiInterceptor
import com.thty.kakaosearchapi.core.net.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single {
        Retrofit.Builder()
                .baseUrl("https://dapi.kakao.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()
    }

    single {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }
        builder.addInterceptor(ApiInterceptor())
        builder.build()
    }

    single {
        get<Retrofit>().create(ApiService::class.java)
    }

}