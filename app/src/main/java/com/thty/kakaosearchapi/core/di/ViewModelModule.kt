package com.thty.kakaosearchapi.core.di

import com.thty.kakaosearchapi.viewmodel.DetailViewModel
import com.thty.kakaosearchapi.viewmodel.MainViewModel
import com.thty.kakaosearchapi.viewmodel.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SearchViewModel(get(), get()) }
    viewModel { MainViewModel(get(), get()) }
    viewModel { DetailViewModel(get(), get()) }
}
