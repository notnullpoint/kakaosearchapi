package com.thty.kakaosearchapi.core.di

import androidx.room.Room
import com.thty.kakaosearchapi.data.room.DocumentDB
import org.koin.dsl.module

val databaseModule = module {

    single {
        Room.databaseBuilder(get(), DocumentDB::class.java, "document.db").build()
    }

    single { get<DocumentDB>().getDocumentDao() }
}