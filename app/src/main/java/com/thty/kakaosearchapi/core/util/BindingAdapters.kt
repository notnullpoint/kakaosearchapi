package com.thty.kakaosearchapi.core.util

import android.widget.ImageView
import androidx.annotation.Keep
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load


@Keep
object BindingAdapters {

    @JvmStatic
    @BindingAdapter("setAdapter")
    fun bindRecyclerAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
        recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImageUrl(imageView: ImageView, url: String?) {
        if(url != null && url.isNotEmpty()) {
            imageView.load(url)
        }
    }

}
