package com.thty.kakaosearchapi.core.net

import com.thty.kakaosearchapi.data.Documents
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/v3/search/book ")
    suspend fun getBooks(
        @Query(value = "query", encoded = true) query: String,
        @Query("page") page: Int,
        @Query("size") size: Int): Documents
}
