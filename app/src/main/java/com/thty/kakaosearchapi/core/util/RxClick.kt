package com.thty.kakaosearchapi.core.util

import android.annotation.SuppressLint
import android.view.View
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

object RxClick {
    /**
     * 중복클릭을 방지한다.
     * @param v view
     * @param onClickListener listener
     */
    @SuppressLint("CheckResult")
    fun dupClick(v: View, onClickListener: View.OnClickListener) {
        v.clicks()
            .throttleFirst(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { onClickListener.onClick(v) }
    }
}