package com.thty.kakaosearchapi.core.net

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ApiInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
                .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
            .addHeader("Authorization", "KakaoAK 990f63fe19863c4f958689ae5220f1bb") //api 토큰
                .url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}