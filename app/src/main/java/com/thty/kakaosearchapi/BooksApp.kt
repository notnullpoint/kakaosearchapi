package com.thty.kakaosearchapi

import android.app.Application
import com.thty.kakaosearchapi.core.di.databaseModule
import com.thty.kakaosearchapi.core.di.networkModule
import com.thty.kakaosearchapi.core.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BooksApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@BooksApp)
            modules(listOf(viewModelModule, networkModule, databaseModule))
        }
    }
}