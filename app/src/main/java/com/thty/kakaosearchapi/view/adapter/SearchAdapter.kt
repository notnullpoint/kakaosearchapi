package com.thty.kakaosearchapi.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thty.kakaosearchapi.core.util.RxClick
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.databinding.LayoutListBookBinding
import com.thty.kakaosearchapi.view.listener.OnBookClick
import kotlin.collections.ArrayList

class SearchAdapter(var listener: OnBookClick) : RecyclerView.Adapter<SearchAdapter.BookHolder>() {

    var items = ArrayList<Document>()

    fun addItem(items: ArrayList<Document>?){
        this.items.addAll(items ?: ArrayList())
        notifyDataSetChanged()
    }

    fun clear(){
        this.items.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookHolder {
        val binding = LayoutListBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BookHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        val document = items[position]
        holder.setItem(document)

        RxClick.dupClick(holder.binding.root, View.OnClickListener {
            listener.onClick(holder.binding.ivBook, document)
        })
    }

    inner class BookHolder(var binding: LayoutListBookBinding) : RecyclerView.ViewHolder(binding.root){

        fun setItem(item : Document){
            binding.item = item
            binding.executePendingBindings()
        }
    }
}