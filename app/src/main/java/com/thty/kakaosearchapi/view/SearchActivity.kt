package com.thty.kakaosearchapi.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thty.kakaosearchapi.BR
import com.thty.kakaosearchapi.R
import com.thty.kakaosearchapi.base.BaseActivity
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.databinding.ActivitySearchBinding
import com.thty.kakaosearchapi.view.cusview.EndlessRecyclerViewScrollListener
import com.thty.kakaosearchapi.viewmodel.SearchViewModel
import ductranit.me.livedatabus.ConsumableEvent
import ductranit.me.livedatabus.LiveDataBus
import kotlinx.coroutines.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchActivity : BaseActivity<ActivitySearchBinding, SearchViewModel>() {

    override val bindingVariable: Int
        get() = BR.vm
    override val layoutId: Int
        get() = R.layout.activity_search
    override val viewModel: SearchViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getDataBinding().etView.apply {
            isFocusableInTouchMode = true
            isFocusable = true

            GlobalScope.launch(Dispatchers.Main) {
                delay(500)
                requestFocus()
                with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager){
                    showSoftInput(this@apply, 0)
                }
            }

            setOnEditorActionListener(object : TextView.OnEditorActionListener{
                override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                        with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager){
                            hideSoftInputFromWindow(windowToken, 0)
                        }
                        clearFocus()
                        viewModel.getSearchApi(1) // 1첫 로드
                        return true
                    }
                    return false
                }
            })
        }

        getDataBinding().rvView.apply {
            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as GridLayoutManager){
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    if(!viewModel.isLastPage()){
                        viewModel.getSearchApi(page + 1) // page 는 0 부터 시작
                    }
                }
            })
        }

       setObserver()
    }

    private fun setObserver() {
        with(viewModel){
            activityToStart.observe(this@SearchActivity, Observer {
                val document = it.first?.tag as Document
                LiveDataBus.publish("detail", ConsumableEvent(value = document))
                val option  = ActivityOptionsCompat.makeSceneTransitionAnimation(this@SearchActivity, it)
                startActivity(Intent(this@SearchActivity, DetailActivity::class.java), option.toBundle())
            })

            toastMessage.observe(this@SearchActivity, Observer {
                Toast.makeText(this@SearchActivity, it, Toast.LENGTH_SHORT).show()
            })
        }

    }
}