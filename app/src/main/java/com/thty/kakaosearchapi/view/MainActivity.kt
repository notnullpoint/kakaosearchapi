package com.thty.kakaosearchapi.view

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import com.thty.kakaosearchapi.BR
import com.thty.kakaosearchapi.R
import com.thty.kakaosearchapi.base.BaseActivity
import com.thty.kakaosearchapi.core.util.RxClick
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.databinding.ActivityMainBinding
import com.thty.kakaosearchapi.viewmodel.MainViewModel
import ductranit.me.livedatabus.ConsumableEvent
import ductranit.me.livedatabus.LiveDataBus
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    val reqestCode = 4000

    override val bindingVariable: Int
        get() = BR.vm
    override val layoutId: Int
        get() = R.layout.activity_main
    override val viewModel: MainViewModel by viewModel()

    private val binding: ActivityMainBinding by lazy {
        getDataBinding()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        RxClick.dupClick(binding.vSearchBar, View.OnClickListener {
            val option  =
                ActivityOptionsCompat
                    .makeSceneTransitionAnimation(this@MainActivity,
                        Pair.create(binding.tvClickView as View, ViewCompat.getTransitionName(binding.tvClickView)!!),
                        Pair.create(binding.ivSearch as View, ViewCompat.getTransitionName(binding.ivSearch)!!)
                    )

            startActivityForResult(Intent(this, SearchActivity::class.java), reqestCode, option.toBundle())
        })

        startAni()
        setObserver()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == reqestCode){ //검색을 들어갔다오면 갱신한다.
            viewModel.getLatelyBooks() //최근 본 책을 갱신한다.
        }
    }

    private fun setObserver() {
        with(viewModel){
            activityToStart.observe(this@MainActivity, Observer {
                val document = it.first?.tag as Document
                LiveDataBus.publish("detail", ConsumableEvent(value = document))
                val option  = ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity, it)
                startActivity(Intent(this@MainActivity, DetailActivity::class.java), option.toBundle())
            })
        }
    }

    private fun startAni() {
        binding.vSearchBar.also{
            it.alpha = 0f
            it.scaleX = 0.5f
            it.scaleY = 0.5f
            binding.vSearchBar.animate().apply {
                duration = 400
                alpha(1f)
                scaleX(1f)
                scaleY(1f)
                start()
                it.visibility = View.VISIBLE
                setListener(object : Animator.AnimatorListener{
                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                        viewModel.startTyping()
                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                })
            }
        }
    }
}