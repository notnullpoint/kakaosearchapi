package com.thty.kakaosearchapi.view.listener

import android.view.View
import com.thty.kakaosearchapi.data.Document

interface OnBookClick {
    fun onClick(view: View, document: Document)
}