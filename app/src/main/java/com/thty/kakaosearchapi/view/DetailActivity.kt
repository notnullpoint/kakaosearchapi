package com.thty.kakaosearchapi.view

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import coil.api.load
import com.thty.kakaosearchapi.BR
import com.thty.kakaosearchapi.R
import com.thty.kakaosearchapi.base.BaseActivity
import com.thty.kakaosearchapi.core.util.RxClick
import com.thty.kakaosearchapi.data.Document
import com.thty.kakaosearchapi.databinding.ActivityDetailBinding
import com.thty.kakaosearchapi.viewmodel.DetailViewModel
import ductranit.me.livedatabus.LiveDataBus
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>() {

    override val bindingVariable: Int
        get() = BR.vm
    override val layoutId: Int
        get() = R .layout.activity_detail
    override val viewModel: DetailViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        RxClick.dupClick(getDataBinding().ivBack, View.OnClickListener {
            onBackPressed()
        })

        LiveDataBus.subscribe("detail", this, Observer {
            it.runAndConsume {
                val document = it.value
                if(document is Document) {
                    getDataBinding().apply {
                        tvTitle.text = document.title
                        tvContents.text = document.contents
                        tvAuthorsName.text = viewModel.getAuthorsName(document.authors)
                        tvPublisherName.text = document.publisher
                        tvPrice.text = viewModel.getPrice(document.price)
                        tvSalePrice.text = viewModel.getPrice(document.salePrice)

                        ivBook.load(document.thumbnail)
                    }
                    viewModel.insertDocument(document)
                }
            }

            LiveDataBus.unregister("detail")
        })
    }

}