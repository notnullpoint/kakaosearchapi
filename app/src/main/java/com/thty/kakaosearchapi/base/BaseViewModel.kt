package com.thty.kakaosearchapi.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class  BaseViewModel(app: Application) : AndroidViewModel(app) {

    protected val mCompositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }
}