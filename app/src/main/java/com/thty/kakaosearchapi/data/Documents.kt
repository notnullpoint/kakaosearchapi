package com.thty.kakaosearchapi.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Documents {
    @SerializedName("documents")
    @Expose
    var documents: ArrayList<Document>? = null
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
}