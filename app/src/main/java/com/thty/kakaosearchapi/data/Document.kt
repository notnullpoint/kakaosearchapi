package com.thty.kakaosearchapi.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

@Entity
class Document: Serializable {

    @SerializedName("isbn")
    @Expose
    @PrimaryKey
    var isbn: String = ""

    @SerializedName("authors")
    @Expose
    var authors: ArrayList<String> = ArrayList()
    @SerializedName("contents")
    @Expose
    var contents: String = ""
    @SerializedName("price")
    @Expose
    var price = 0
    @SerializedName("publisher")
    @Expose
    var publisher: String = ""
    @SerializedName("sale_price")
    @Expose
    var salePrice = 0
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String = ""
    @SerializedName("title")
    @Expose
    var title: String = ""

    var creationDate: Date = Date(System.currentTimeMillis())
}