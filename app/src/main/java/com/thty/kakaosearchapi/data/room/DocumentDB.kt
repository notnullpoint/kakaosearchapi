package com.thty.kakaosearchapi.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.thty.kakaosearchapi.data.Document

@Database(entities = [Document::class], version = 1)
@TypeConverters(StringArrayConverter::class, DateConverter::class)
abstract class DocumentDB: RoomDatabase(){
    abstract fun getDocumentDao(): BookDao
}