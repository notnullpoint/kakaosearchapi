package com.thty.kakaosearchapi.data.room

import androidx.room.TypeConverter

class StringArrayConverter {

    @TypeConverter
    fun toList(strings: String): ArrayList<String> {
        val list = ArrayList<String>()
        val array = strings.split(",")
        for (s in array) {
            list.add(s)
        }
        return list
    }

    @TypeConverter
    fun toString(strings: ArrayList<String>): String {
        var result = ""
        strings.forEachIndexed { index, element ->
            result += element
            if(index != (strings.size-1)){
                result += ","
            }
        }
        return result
    }
}