package com.thty.kakaosearchapi.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thty.kakaosearchapi.data.Document

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg person: Document)

    @Query("SELECT * FROM document ORDER BY creationDate DESC LIMIT 5")
    suspend fun getDocuments(): List<Document>
}