package com.thty.kakaosearchapi.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Meta {
    @SerializedName("is_end")
    @Expose
    var isEnd = false
    @SerializedName("pageable_count")
    @Expose
    var pageableCount = 0
    @SerializedName("total_count")
    @Expose
    var totalCount = 0
}